package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class TelevisionTest {
	
	Television television = new Television();
	
	@Before
	public void setup() {
		
		
	}
	
	@Test
	public void checking_limits_on_volume() {
		if(television.getCurrentVolume() > 10 || television.getCurrentVolume() < 0) {
			Assert.fail();
		}
	}
	

	
	@Test
	public void volume_resets_correctly() {
		
		if (television.IsOn() == true) {
		Assert.assertEquals(2, television.getCurrentVolume());
		}
	}
	
	@Test
	public void checking_for_side_effects() {
		if (television.getCurrentVolume() == 4) {
			Assert.assertEquals(4, television.getCurrentVolume());
		}
		if (television.getSelectedChannel() == 8) {
			Assert.assertEquals(8, television.getSelectedChannel());
		}
		
	}

}
