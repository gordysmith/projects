package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeTest {
	
	private Employee employee; {
		
	}
	
	@Before
	public void setup() {
		employee = new Employee(22, "John", "Smith", 45000);
	}
	
	@Test
	public void does_raise_work() {
		//Arrange
		employee.RaiseSalary(5);
		//Act
		double raiseSalary = employee.getAnnualSalary();
		//Assert
		Assert.assertEquals(47250, raiseSalary, 0.001);
	}
	
	@Test
	public void check_for_negative_percentage() {
		//Arrange
				employee.RaiseSalary(-5);
				//Act
				double raiseSalary = employee.getAnnualSalary();
				//Assert
				Assert.assertEquals(47250, raiseSalary, 0.001);
	}
	

}
