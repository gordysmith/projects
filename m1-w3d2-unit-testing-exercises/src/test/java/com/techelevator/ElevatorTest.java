package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class ElevatorTest {
	
	Elevator elevator;
	
	@Before
	public void setup() {
		elevator = new Elevator(2, 13);
	}
	
	@Test
	public void elevator_should_not_go_up_with_door_open() {
	//Arrange (Open the door)
	elevator.isDoorOpen();
	
	//Act (Test - Try to move the Elevator)
	elevator.GoUp(5);
	
	int currentFloor = elevator.getCurrentLevel();
	
	//Assert 
	Assert.assertEquals(5, currentFloor);
	
	
	
	}
	
	@Test
	public void elavator_should_not_go_through_ceiling() {
	
	if (elevator.getNumberOfLevels()>10) {
		Assert.fail();
	}
		
	}
	
	@Test
	public void testing_random_side_effects() {
		elevator.CloseDoor();
		elevator.GoUp(10);
		elevator.OpenDoor();
		Assert.assertEquals(10, elevator.getCurrentLevel());
		
	}

}
