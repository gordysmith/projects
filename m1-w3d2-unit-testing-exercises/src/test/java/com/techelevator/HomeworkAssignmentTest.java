package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;

public class HomeworkAssignmentTest {
	
	private HomeworkAssignment homeworkAssingment;
	
	@Before
	public void setup() {
		homeworkAssingment = new HomeworkAssignment(100);
		
	}
	
	@Test
	public void is_grade_a() {
		
		String letterGrade = homeworkAssingment.getLetterGrade();
		
		if(homeworkAssingment.getTotalMarks() >= 90 && homeworkAssingment.getTotalMarks() <= 100) {
			Assert.assertEquals("D", letterGrade);
		}
		
	}
	
	@Test
	public void is_grade_b() {
		String letterGrade = homeworkAssingment.getLetterGrade();
		
		if(homeworkAssingment.getTotalMarks() >= 80 && homeworkAssingment.getTotalMarks() < 90) {
			Assert.assertEquals("B", letterGrade);
		}

	}
	
	@Test
	public void is_grade_c() {
		String letterGrade = homeworkAssingment.getLetterGrade();
		
		if(homeworkAssingment.getTotalMarks() >= 70 && homeworkAssingment.getTotalMarks() < 80) {
			Assert.assertEquals("C", letterGrade);
		}
	
	}
	
	@Test
	public void is_grade_d() {
		
		String letterGrade = homeworkAssingment.getLetterGrade();
		
		if(homeworkAssingment.getTotalMarks() >= 60 && homeworkAssingment.getTotalMarks() < 70) {
			Assert.assertEquals("D", letterGrade);
		}
		//arrange
//		homeworkAssingment.setTotalMarks(60);
//		//act
//		String letterGrade = homeworkAssingment.getLetterGrade();
//		//assert
//		Assert.assertEquals("D", letterGrade);
//	
	}
	
	@Test
	public void is_grade_f() {
		//arrange
		homeworkAssingment.setTotalMarks(50);
		//act
		String letterGrade = homeworkAssingment.getLetterGrade();
		//assert
		Assert.assertEquals("F", letterGrade);
	}
	
	@Test
	public void given_incorrect_value_for_total_marks() {
		
		homeworkAssingment.setTotalMarks(95);
		Assert.assertEquals("B", homeworkAssingment.getLetterGrade());
		
	}
	
}
