package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;

public class SmartPhoneTest {
	
	SmartPhone smartPhone;
	String number;
	String carrier;
	
	@Before
	public void setup() {
		smartPhone = new SmartPhone("1234567890", "verizon");
	}
	
	@Test
	public void battery_recharged() {
		smartPhone.getBatteryCharge();
		smartPhone.RechargeBattery();
		Assert.assertEquals(95, smartPhone.getBatteryCharge());
	}
	
	@Test
	public void does_battery_drain() {
		smartPhone.Call("1234567890", 150);
		Assert.assertEquals(-50, smartPhone.getBatteryCharge());
	}
	
	@Test
	public void enought_battery_for_talking_minutes() {
		smartPhone.isOnCall();
		int noBattery = 0;
		smartPhone.getBatteryCharge();
		Assert.assertEquals(!smartPhone.isOnCall(), smartPhone.isOnCall());
		
	}
	
	@Test
	public void calling_7_digit_number() {
		Assert.assertEquals(smartPhone.Call("1234567890", 45), smartPhone.Call("1234567", 45));
	}
	
	
	

}
