package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AirplaneTest {

	Airplane airplane;


@Before
public void setup() {
	airplane = new Airplane(100, 100);
}

@Test 
public void can_i_overbook() {
	//Arrange
	boolean overBooked = airplane.Reserve(true, 101);
	//Act
	//Assert
	Assert.assertFalse(overBooked);
}

@Test
public void is_first_class_accurate() {
	
	
}

@Test
public void is_coach_count_accurate() {
	int availableFirst = airplane.getTotalCoachSeats() - airplane.getBookedCoachSeats();
	Assert.assertEquals(airplane.getAvailableCoachSeats(), availableFirst);
}

@Test
public void is_first_count_accurate_after_booking() {
	//Arrange
	int count = airplane.getAvailableFirstClassSeats() + airplane.getBookedFirstClassSeats();
	Assert.assertEquals(airplane.getBookedFirstClassSeats(), count);
	
	
}

}


