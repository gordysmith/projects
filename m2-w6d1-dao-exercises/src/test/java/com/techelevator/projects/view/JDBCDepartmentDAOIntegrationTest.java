package com.techelevator.projects.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.jdbc.JDBCDepartmentDAO;



public class JDBCDepartmentDAOIntegrationTest {
	
	private static final long TEST_DEPARTMENT_ID = 123;
	
	private static SingleConnectionDataSource dataSource;
	private JDBCDepartmentDAO dao;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		dataSource.setAutoCommit(false);
	}	
	
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	
	@Before
	public void setup() {
		String sqlInsertDepartment = "INSERT INTO department (department_id, name) VALUES (?, 'Gordys Department')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertDepartment, TEST_DEPARTMENT_ID);
		dao = new JDBCDepartmentDAO(dataSource);
	}
	
	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	@Test
	public void get_list_of_all_departments() {
		List<Department> results = dao.getAllDepartments();
		
		assertNotNull(results);
		assertTrue(results.size()>=1);
	}
	
	@Test 
	public void search_dept_by_name() {
		Department thisDepartment = getDepartment(TEST_DEPARTMENT_ID, "test");
		dao.saveDepartment(thisDepartment);
		List<Department> results = dao.searchDepartmentsByName("test");
		
		
		assertNotNull(results);
		assertEquals(1, results.size());
		Department saveDepartment = results.get(0);
		assertDepartmentsAreEqual(thisDepartment, saveDepartment);
	}
	
	@Test 
	public void save_department () {
		Department savedDepartment = getDepartment(TEST_DEPARTMENT_ID, "saved");
		dao.saveDepartment(savedDepartment);
		Department didItSave = dao.getDepartmentById(TEST_DEPARTMENT_ID);
		
		assertNotEquals(null, savedDepartment.getId());
		assertDepartmentsAreEqual(savedDepartment, didItSave);
	}
	
	@Test
	public void create_department() {
		Department newDepartment = getDepartment(TEST_DEPARTMENT_ID, "created");
		dao.createDepartment(newDepartment);
		Department didItCreate = dao.getDepartmentById(newDepartment.getId());
		
		assertNotEquals(null, newDepartment.getId());
		assertDepartmentsAreEqual(newDepartment, didItCreate);
		
	}
	
	@Test
	public void get_dept_by_id() {
		Department idSearch = getDepartment(TEST_DEPARTMENT_ID, "id");
		dao.saveDepartment(idSearch);
		Department isIdThere = dao.getDepartmentById(TEST_DEPARTMENT_ID);
		
		assertNotNull(isIdThere);
		assertDepartmentsAreEqual(idSearch, isIdThere);
	}
	
	private Department getDepartment (long id, String name) {
		Department department = new Department();
		department.setId(id);
		department.setName(name);
		return department;
	}
	
	private void assertDepartmentsAreEqual(Department expected, Department actual) {
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getId(), actual.getId());
	}

}

