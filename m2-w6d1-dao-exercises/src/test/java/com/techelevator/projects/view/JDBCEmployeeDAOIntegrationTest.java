package com.techelevator.projects.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.jdbc.JDBCEmployeeDAO;

public class JDBCEmployeeDAOIntegrationTest {
	
	private static final long TEST_EMPLOYEE_ID = 123;
	
	private static SingleConnectionDataSource dataSource;
	private JDBCEmployeeDAO dao;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		dataSource.setAutoCommit(false);
	}	
	
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	
	@Before
	public void setup() {
		String sqlInsertEmployee = "INSERT INTO employee (employee_id, department_id, first_name, last_name, birth_date, gender, hire_date) VALUES (?, 4, 'Sufjan', 'Stevens', '1983-10-02', 'M', '2012-08-12')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertEmployee, TEST_EMPLOYEE_ID);
		dao = new JDBCEmployeeDAO(dataSource);
	}
	
	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();	
	}
	
	@Test
	public void search_by_employee_name() {
		Employee thisEmployee = getEmployee(TEST_EMPLOYEE_ID, 4,  "Sufjan", "Stevens", LocalDate.of(1983, 10, 02), 'M', LocalDate.of(2012, 8, 12));
		List<Employee> results = dao.searchEmployeesByName("Sufjan", "Stevens");
		
		assertNotNull(results);
		assertEquals(1, results.size());
		//assertEquals(123, TEST_EMPLOYEE_ID);
		Employee newEmployee = results.get(0);
		assertEmployeesAreEqual(thisEmployee, newEmployee);
		
	}
	
	private Employee getEmployee (long employeeId, long departmentId, String firstName, String lastName, LocalDate birthDay, char gender, LocalDate hireDate) {
		Employee employee = new Employee();
		employee.setId(employeeId);
		employee.setDepartmentId(departmentId);
		employee.setFirstName(firstName);
		employee.setLastName(lastName);
		employee.setBirthDay(birthDay);
		employee.setGender(gender);
		employee.setHireDate(hireDate);
		return employee;
	}
	
	private void assertEmployeesAreEqual(Employee expected, Employee actual) {
		assertEquals(expected.getFirstName(), actual.getFirstName());
		assertEquals(expected.getLastName(), actual.getLastName());
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getDepartmentId(), actual.getDepartmentId());
	}
	
	

}
