package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.EmployeeDAO;

public class JDBCEmployeeDAO implements EmployeeDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCEmployeeDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Employee> getAllEmployees() {
		List<Employee> employees = new ArrayList<Employee>();
		String selectAllEmployees = "select * from employee";
		SqlRowSet result = jdbcTemplate.queryForRowSet(selectAllEmployees);

		while (result.next()) {
			employees.add(mapRowToEmployee(result));
		}

		return employees;
	}

	@Override
	public List<Employee> searchEmployeesByName(String firstNameSearch, String lastNameSearch) {
		List<Employee> employeeSearch = new ArrayList<Employee>();
		String sqlFindEmployeeName= "select * from employee where first_name ILIKE ? and last_name ILIKE ?";
		//String lastName = "select * from employee\n" + "where employee.last_name LIKE ?";

		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlFindEmployeeName, "%" + firstNameSearch + "%", "%" + lastNameSearch + "%");
		//SqlRowSet lastNameResult = jdbcTemplate.queryForRowSet(lastName, "%" + lastNameSearch + "%");

		while (result.next()) {
			//Employee employee = mapRowToEmployee(result);
			employeeSearch.add(mapRowToEmployee(result));
		}

//		while (lastNameResult.next()) {
//			employeeSearch.add(mapRowToEmployee(lastNameResult));
//		}

		return employeeSearch;

	}

	@Override
	public List<Employee> getEmployeesByDepartmentId(long id) {
		List<Employee> employeesByDeptId = new ArrayList<Employee>();
		String getIdStatement = "select * from employee " + "where department_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(getIdStatement, id);
		if (result.next()) {
			employeesByDeptId.add(mapRowToEmployee(result));
		}
		return employeesByDeptId;
	}

	@Override
	public List<Employee> getEmployeesWithoutProjects() {
		List<Employee> employeesWithoutProject = new ArrayList<Employee>();
		String noProjects = "select * from employee " + 
				"full outer join project_employee on employee.employee_id = project_employee.employee_id " + 
				"where project_id is null";
		SqlRowSet result = jdbcTemplate.queryForRowSet(noProjects);
		while (result.next()) {
			employeesWithoutProject.add(mapRowToEmployee(result));
		}
		return employeesWithoutProject;
	}

	@Override
	public List<Employee> getEmployeesByProjectId(Long projectId) {
		List<Employee> employeesByProjectId = new ArrayList<Employee>();
		String getEmployeesOnProject = "select * from employee " + 
				"join project_employee on employee.employee_id = project_employee.employee_id " + 
				"where project_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(getEmployeesOnProject, projectId);
		if (result.next()) {
			employeesByProjectId.add(mapRowToEmployee(result));
		}
		return employeesByProjectId;
	}

	@Override
	public void changeEmployeeDepartment(Long employeeId, Long departmentId) {

		String changeDepartment = "update employee " + "set department_id = ? " + "where employee_id = ? ";

		jdbcTemplate.update(changeDepartment, employeeId, departmentId);

	}

	private Employee mapRowToEmployee(SqlRowSet result) {
		Employee employee = new Employee();
		employee.setId(result.getLong("employee_id"));
		employee.setDepartmentId(result.getLong("department_id"));
		employee.setFirstName(result.getString("first_name"));
		employee.setLastName(result.getString("last_name"));
		if (result.getDate("birth_date") != null) {
			employee.setBirthDay(result.getDate("birth_date").toLocalDate());
		}
		employee.setGender(result.getString("gender").charAt(0));
		if (result.getDate("hire_date") != null) {
			employee.setHireDate(result.getDate("hire_date").toLocalDate());
		}
		return employee;
	}

}
