package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.DepartmentDAO;

public class JDBCDepartmentDAO implements DepartmentDAO {
	
	private JdbcTemplate jdbcTemplate;

	public JDBCDepartmentDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Department> getAllDepartments() {
		
		List<Department> departments = new ArrayList<Department>();                //Creates new Department list of departments
		String selectAllDepartments = "select * from department";                  //Creates a string in a variable
		SqlRowSet result = jdbcTemplate.queryForRowSet(selectAllDepartments);      //
		
		while(result.next()) {
			departments.add(mapRowToDepartment(result));
		}
		
		return departments;
	}
	


	@Override
	public List<Department> searchDepartmentsByName(String nameSearch) {
		List<Department> nameSearches = new ArrayList<Department>();
		String searchResults = "select * from department\n" + 
				"where department.name LIKE ?";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(searchResults, "%" + nameSearch + "%");
		
		while (result.next()) {
			nameSearches.add(mapRowToDepartment(result));
		}
		
		
		return nameSearches;
	}

	@Override
	public void saveDepartment(Department updatedDepartment) {
		
		String updateStatement = "update department \n" + 
				"set name = ?\n" + 
				"where department_id = ?";
		
		jdbcTemplate.update(updateStatement, updatedDepartment.getName(), updatedDepartment.getId());
	}

	@Override
	public Department createDepartment(Department newDepartment) {
		
		String createStatement = "insert into department (department_id, name) " + 
				"values (?,?)";
		newDepartment.setId(getNextDeptId());
		jdbcTemplate.update(createStatement, newDepartment.getId(), newDepartment.getName());
		
		return newDepartment;
	}

	@Override
	public Department getDepartmentById(Long id) {
		Department idName = new Department();
		String getIdStatement = "select * from department " + 
				"where department_id = ?";
		SqlRowSet result = jdbcTemplate.queryForRowSet(getIdStatement, id);
		if (result.next()) {
			idName = mapRowToDepartment(result);
		}
		
		return idName;
	}
	
	
	
	private Department mapRowToDepartment(SqlRowSet result) {
		Department department = new Department();
		department.setId(result.getLong("department_id"));
		department.setName(result.getString("name"));
		return department;
	}
	private long getNextDeptId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("select nextval('seq_department_id')");
		
		if (nextIdResult.next()) {
			return nextIdResult.getLong(1);
		} else {
			throw new RuntimeException ("An error occured while getting the id for the department.");
		}
		
	}

}
