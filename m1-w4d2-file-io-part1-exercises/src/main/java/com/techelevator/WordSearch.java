package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordSearch {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter a search word: ");
		String searchWord = in.nextLine();
		
		System.out.println("Should the search be case sensitive? (Y) or (N)");
		String caseChoice = in.next();
		int lineCount = 0;
		
		File inputFile = getInputFileFromUser();
		
		if (caseChoice.contains("N")) {
		try(Scanner fileScanner = new Scanner(inputFile)) {
			while(fileScanner.hasNextLine()) {
				String line = fileScanner.nextLine();
				lineCount ++;
				if (line.contains(searchWord)) {
					System.out.println("I've found " + searchWord + " on line " + lineCount + " : " + line);
				}
				
				
			}
		
		}
		}
		
		if (caseChoice.contains("Y")) {
			try(Scanner fileScanner = new Scanner(inputFile)) {
				while(fileScanner.hasNextLine()) {
				String line = fileScanner.nextLine();
				lineCount ++;
				searchWord = searchWord.substring(0).toUpperCase();
				if (line.contains(searchWord.toUpperCase())) {
						System.out.println("I've found " + searchWord + " on line " + lineCount + " : " + line);
					}
					
					
				}
			
			}
			
		}
	}

	@SuppressWarnings("resource")
	private static File getInputFileFromUser() {
		Scanner userInput = new Scanner(System.in);
		System.out.print("Please enter path to input file >>> ");
		String path = userInput.nextLine();
		
		File inputFile = new File(path);
		if(inputFile.exists() == false) { // checks for the existence of a file
			System.out.println(path+" does not exist");
			System.exit(1); // Ends the program
		} else if(inputFile.isFile() == false) {
			System.out.println(path+" is not a file");
			System.exit(1); // Ends the program
		}
		return inputFile;
	}

}
