--create database project_organizer
drop table employee cascade;
drop table department cascade;
drop table project cascade;
drop table employee_department;
drop table employee_project;

create table employee (
        employee_id serial,
        job_title varchar(100) not null,
        last_name varchar(35) not null,
        first_name varchar(35) not null,
        gender varchar (35) null,
        birth_date date not null,
        hire_date date not null,
        department varchar(50) not null,
        
        constraint pk_employee_id primary key (employee_id)
        

);

create table department (
        department_number serial,
        department_name varchar(50) not null,
        
        
        constraint pk_department_number primary key (department_number)
        
);

create table employee_department (
        employee_id int not null,
        department_number int not null,
        
        constraint fk_department_number foreign key (department_number) references department (department_number),
        constraint fk_employee_id foreign key (employee_id) references employee (employee_id)
);

create table project (
        project_number serial,
        project_name varchar(100) not null,
        start_date date not null,
        
        
        constraint pk_project_number primary key (project_number)
        
);
 
 create table employee_project (
        employee_id int not null,
        project_number int not null,
        
        constraint fk_project_number foreign key (project_number) references project (project_number),
        constraint fk_employee_id foreign key (employee_id) references employee (employee_id)
 );
 
select * from employee

insert into employee (job_title, last_name, first_name, gender, birth_date, hire_date, department)
values ('Supervisor', 'Jones', 'Tom', 'M', '1983-10-02', '2012-08-01', 'Sales');

insert into employee (job_title, last_name, first_name, gender, birth_date, hire_date, department)
values ('Associate', 'Brown', 'Julie', 'F', '1991-09-12', '2015-03-25', 'Sales');

insert into employee (job_title, last_name, first_name, gender, birth_date, hire_date, department)
values ('Assistant to the Regional Manager', 'Shrute', 'Dwight', 'M', '1974-06-23', '2011-12-12', 'Sales');

insert into employee (job_title, last_name, first_name, gender, birth_date, hire_date, department)
values ('Head of HR', 'Flax', 'Holly', 'F', '1978-11-15', '2009-08-05', 'Human Resources');

insert into employee (job_title, last_name, first_name, gender, birth_date, hire_date, department)
values ('The Worst', 'Toby', 'Flenderson', 'M', '1971-10-31', '2005-08-05', 'Human Resources');

insert into employee (job_title, last_name, first_name, gender, birth_date, hire_date, department)
values ('Accountant', 'Martinez', 'Oscar', 'M', '1972-1-11', '2008-09-25', 'Accounting');

insert into employee (job_title, last_name, first_name, gender, birth_date, hire_date, department)
values ('Accountant', 'Malone', 'Kevin', 'M', '1968-11-13', '2004-11-23', 'Accounting');

insert into employee (job_title, last_name, first_name, gender, birth_date, hire_date, department)
values ('Accountant', 'Shrute', 'Angela', 'F', '1978-06-08', '2008-04-01', 'Accounting');

select * from department 

insert into department (department_name)
values ('Sales');

insert into department (department_name)
values ('Human Resources');

insert into department (department_name)
values ('Accounting');

select * from employee_department

insert into employee_department(employee_id, department_number)
values (1, 1);

insert into employee_department(employee_id, department_number)
values (2, 1);

insert into employee_department(employee_id, department_number)
values (3, 1);

insert into employee_department(employee_id, department_number)
values (4, 2);

insert into employee_department(employee_id, department_number)
values (5, 2);

insert into employee_department(employee_id, department_number)
values (6, 3);

insert into employee_department(employee_id, department_number)
values (7, 3);

insert into employee_department(employee_id, department_number)
values (8, 3);

select * from project

insert into project (project_name, start_date)
values ('Sales Campaign', '2018-05-21');

insert into project (project_name, start_date)
values ('Budget Campaign', '2018-01-01');

insert into project (project_name, start_date)
values ('Diversity Training', '2018-08-15');

insert into project (project_name, start_date)
values ('Wellness Program', '2018-01-01');

select * from employee_project

insert into employee_project (employee_id, project_number)
values (2, 1);

insert into employee_project (employee_id, project_number)
values (3, 1);

insert into employee_project (employee_id, project_number)
values (6, 2);

insert into employee_project (employee_id, project_number)
values (8, 2);

insert into employee_project (employee_id, project_number)
values (4, 3);

insert into employee_project (employee_id, project_number)
values (5, 3);

insert into employee_project (employee_id, project_number)
values (1, 4);

insert into employee_project (employee_id, project_number)
values (7, 4);


