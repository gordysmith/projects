<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<c:import url="/WEB-INF/jsp/header.jsp">
		<c:param name="subTitle">Your Favorite Things</c:param>
	</c:import>
	
	<h2>What's your favorite season?</h2>
	
	<c:url var = "favoriteSeason" value = "/favoriteSeason" />
	<form action = "${favoriteSeason}" method = "POST">
		<label for="favoriteSeason">What is your favorite season?</label> <br/>
		<input type = "radio" name= "favoriteSeason" value="winter" checked = "checked"> Winter <br/>
		<input type = "radio" name= "favoriteSeason" value="spring"> Spring <br/>
		<input type = "radio" name= "favoriteSeason" value="summer"> Summer <br/>
		<input type = "radio" name= "favoriteSeason" value="fall"> Fall <br/>
		<input type = "submit" value = "Next >>>" />
	</form>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
	<c:import url="/WEB-INF/jsp/footer.jsp"/>