<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<c:import url="/WEB-INF/jsp/header.jsp">
		<c:param name="subTitle">Your Favorite Things</c:param>
	</c:import>
	
	<h2>Thanks for taking our survey! Here are your favorite things!</h2>
	
	<h3>Your favorite color is: "${favorites.favoriteColor}"</h3>
	
	<h3>Your favorite food is: "${favorites.favoriteFood}"</h3>
	
	<h3>Your favorite season is: "${favorites.favoriteSeason}"</h3>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
	<c:import url="/WEB-INF/jsp/footer.jsp"/>