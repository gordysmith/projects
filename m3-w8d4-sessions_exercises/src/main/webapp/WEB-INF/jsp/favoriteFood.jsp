<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<c:import url="/WEB-INF/jsp/header.jsp">
		<c:param name="subTitle">Your Favorite Things</c:param>
	</c:import>
	
	<h2>What is your favorite food?</h2>
	
		<c:url var = "favoriteFood" value = "/favoriteFood"/>
		<form action = "${favoriteFood}" method = "POST">
			<label for="favoriteFood">What is your favorite food?</label>
			<input type = "text" name = favoriteFood />
			<input type = "submit" value = "Next >>>" />
		</form>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
	<c:import url="/WEB-INF/jsp/footer.jsp"/>