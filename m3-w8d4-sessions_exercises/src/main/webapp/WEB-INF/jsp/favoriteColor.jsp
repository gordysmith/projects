<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<c:import url="/WEB-INF/jsp/header.jsp">
		<c:param name="subTitle">Your Favorite Things</c:param>
	</c:import>
	
	<h2>What is your favorite color?</h2>
	
		<c:url var = "favoriteColor" value = "/favoriteColor"/>
		<form action = "${favoriteColor}" method = "POST">
			<label for="favoriteColor">What is your favorite color?</label>
			<input type = "text" name = favoriteColor />
			<input type = "submit" value = "Next >>>"/>
		</form>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
	<c:import url="/WEB-INF/jsp/footer.jsp"/>
