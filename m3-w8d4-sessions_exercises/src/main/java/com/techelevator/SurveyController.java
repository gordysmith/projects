package com.techelevator;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller 
@SessionAttributes("favorites")

public class SurveyController {

	@RequestMapping(path="/favoriteColor", method=RequestMethod.GET)
	public String favoriteColor() {
		
		return "favoriteColor";
	}
	
	@RequestMapping(path="/favoriteColor", method=RequestMethod.POST)
	public String storeFavoriteColor(@RequestParam String favoriteColor, ModelMap model) {
		
		FavoriteThing answer1 = new FavoriteThing();
		
		answer1.setFavoriteColor(favoriteColor);
		
		model.addAttribute("favorites", answer1);
		
		return "redirect:/favoriteFood";
	}
	
	@RequestMapping(path="/favoriteFood", method=RequestMethod.GET)
	public String favoriteFood() {
		
		return "favoriteFood";
	}
	
	@RequestMapping(path="/favoriteFood", method=RequestMethod.POST)
	public String storeFavoriteFood(@RequestParam String favoriteFood, ModelMap model) {
		
		FavoriteThing answer2 = (FavoriteThing)model.get("favorites");
		
		answer2.setFavoriteFood(favoriteFood);
		
		return "redirect:/favoriteSeason";
	}
	
	@RequestMapping(path="/favoriteSeason", method=RequestMethod.GET)
	public String favoriteSeason() {
		
		return "favoriteSeason";
	}
	
	@RequestMapping(path="/favoriteSeason", method=RequestMethod.POST)
	public String storeFavoriteSeason(@RequestParam String favoriteSeason, ModelMap model) {
		
		FavoriteThing answer3 = (FavoriteThing)model.get("favorites");
		
		answer3.setFavoriteSeason(favoriteSeason);
		
		return "redirect:/favoriteResults";
	}
	
	@RequestMapping(path="/favoriteResults", method=RequestMethod.GET)
	public String favoriteResults() {
		
		return "favoriteResults";
	}	
}
