﻿/// <reference path="../jquery-3.1.1.js" />
$(document).ready(function () {
    $("#BillingPostalCode").on("blur", function (event){
    	$.ajax({
    		url: "../api/getTax",
    		data: {
    			billingZipCode: $("#BillingPostalCode").val(),
    			subtotal: $("#order-summary #subtotal span")[0].innerText.substring(1),	
    		},
    		type: "GET",
    		dataType: "json"
    	}).done(function (data){
    		$("#order-summary #tax span").text("$" + data);
    	}).fail(function(xhr, status, error){
    		console.log(error);
    	});
    });
});