﻿/// <reference path="../jquery-3.1.1.js" />
/// <reference path="../jquery.validate.js" />

$(document).ready(function () {

	$("#form0").validate({
		
		debug: false, 
		rules: {
			email: {
			
				email: true
			},
			
			password: {
				
				required: true,
				minlength: 8
			},
			
			confirmPassword : {
				
				required: true,
				equalTo: "#password"
			},
		},
		
		messages: {
			email: "Enter a valid email"
		}, 
		
		errorClass: "field-validation-error",
		validClass: "valid"
		
		
		
	});
	
	$("#checkout").validate({
		
		debug: false,
		rules: {
			BillingAddress1: {
				required: true
			},
			
			BillingCity: {
				required: true
			},
			
			BillingState: {
				required: true
			},
			
			BillingPostalCode: {
				required: true
			},
			
			ShippingAddress1: {
				required: true
			}, 
			
			ShippingCity: {
				required: true
			}, 
			
			ShippingState: {
				required: true
			}, 
			
			ShippingPostalCode: {
				required: true
			}, 
			
			NameOnCard: {
				required: true,
				minlength: 3,
				maxlenght: 28
			}
			
		},
		
		messages: {
			BillingAddress1: { 
				required:"Address Required"
			},	
			
			BillingCity: {
				required: "City Required"
			}, 
			
			BillingState: {
				required: "State Required"
			},
			
			BillingPostalCode: {
				required: "Postal Code Required" 
			}, 
			
			ShippingAddress1: {
				required: "Shipping Address Required"
			},
			
			ShippingCity: {
				required: "Shipping City Required"
			}, 
			
			ShippingState: {
				required: "Shipping State Required"
			}, 
			
			ShippingPostalCode: {
				required: "Shipping Postal Code Required"
			},
			
			NameOnCard: {
				required: "Name Required"
			}
			
				
		}
	});
			
    // Email address - required
    // Email address - .gov only (BONUS)
    // Password - required, length 8 or more
    // ConfirmPassword
});