<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>

<html>
<head>
<meta name="viewport" content="width=device-width" />
<title>Product Tiles View</title>
<link rel="stylesheet" href="css/site.css" />
</head>
<body>
	<header>
		<h1>MVC Exercises - Views Part 2: Models</h1>
	</header>
	<nav>
	<c:url var ="productListURL" value = "/productList" />
		<ul>
			<li><a href="${productListURL}">List Layout</a></li>
			<li><a href="/productTiles">Tile Layout</a></li>
			<li><a href="/productTable">Table Layout</a></li>
		</ul>


	</nav>
	<section id="main-content">

		<table>
			<tr>
				<td></td>
				<c:forEach var="product" items="${productList}">
					<th><img class="tableImages" src="img/${product.imageName}" />
					</th>
				</c:forEach>
			</tr>

			<tr>
				<td>Name</td>
				<c:forEach var="product" items="${productList}">
					<td><c:out value="${product.name}" /></td>
				</c:forEach>
			</tr>

			<tr>
				<td>Rating</td>
				<c:forEach var="product" items="${productList}">
					<td><c:choose>
							<c:when test="${product.averageRating < 2.0}">
								<img class="stars" src="img/1-star.png" />
							</c:when>

							<c:when test="${product.averageRating < 3.0}">
								<img class="stars" src="img/2-star.png" />
							</c:when>

							<c:when test="${product.averageRating < 4.0}">
								<img class="stars" src="img/3-star.png" />
							</c:when>

							<c:when test="${product.averageRating < 5.0}">
								<img class="stars" src="img/4-star.png" />
							</c:when>

							<c:when test="${product.averageRating < 6.0}">
								<img class="stars" src="img/5-star.png" />
							</c:when>

						</c:choose></td>
				</c:forEach>
			</tr>
			
			<tr>
				<td>Mfr</td>
				<c:forEach var="product" items="${productList}">
					<td><c:out value="${product.manufacturer}" /></td>
				</c:forEach>
			</tr>
			
			<tr>
				<td>Price</td>
				<c:forEach var="product" items="${productList}">
					<td><c:out value="$${product.price}" /></td>
				</c:forEach>
			</tr>
			
				<tr>
				<td>WeightInLbs</td>
				<c:forEach var="product" items="${productList}">
					<td><c:out value="${product.weightInLbs}" /></td>
				</c:forEach>
			</tr>


		</table>



	</section>
</body>
</html>
