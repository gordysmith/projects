<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Product List View</title>
    <link rel="stylesheet" href="css/site.css" />
</head>
<body>
    <header>
        <h1>MVC Exercises - Views Part 2: Models</h1>        
    </header>
    <nav>
    	<c:url var ="productListURL" value = "/productList.jsp" />
        <ul>
            <li><a href="${productListURL}">List Layout</a></li>
            <li><a href="/productTiles">Tile Layout</a></li>
            <li><a href="/productTable">Table Layout</a></li>
        </ul>
        

    </nav>
    <section id="main-content">

		<h1>Toy Department</h1>
		

		<c:forEach var = "product" items = "${productList}">
			
				
				<img class="images" src="img/${product.imageName}" />
				

				<div class="product-info">
					
						<span id = "name">${product.name}</span>
						</br>
						<span id = "manufacturer"> by ${product.manufacturer}</span>
						</br>
						<span id = "price">$${product.price}</span>
						</br>
						<span id = "weight">${product.weightInLbs}</span>
						</br>
						<c:choose>
							<c:when test = "${product.averageRating < 2.0}" >
								<img class = "stars" src = "img/1-star.png" />
							</c:when>
							
							<c:when test = "${product.averageRating < 3.0}" >
								<img class = "stars" src = "img/2-star.png" />
							</c:when>
							
							<c:when test = "${product.averageRating < 4.0}" >
								<img class = "stars" src = "img/3-star.png" />
							</c:when>
							
							<c:when test = "${product.averageRating < 5.0}" >
								<img class = "stars" src = "img/4-star.png" />
							</c:when>
							
							<c:when test = "${product.averageRating < 6.0}" >
								<img class = "stars" src = "img/5-star.png" />
							</c:when>
							
						</c:choose>
						
					

				</div>
			
		</c:forEach>
			
	</section>
</body>
</html>