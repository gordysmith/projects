package com.techelevator;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class DeliveryDriverDemo {
	
	 

	public static void main(String[] args) {
		
		
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Please enter a distance");
		Double distance = in.nextDouble();
		in.nextLine();
		System.out.println("Please enter a weight");
		Double weight = in.nextDouble();
		in.nextLine();		
		System.out.println("Are you using (P)ounds or (O)unces?");
		String choice = in.nextLine();
			if (choice.equalsIgnoreCase("P"))
				weight = weight * 16;
			
//		DeliveryDriver fedex = new FexEd();	
//		Double rate = fedex.calculateRate(distance, weight);
//		
//		System.out.println(fedex.getName() + " " + fedex.calculateRate(distance, weight));
//		
//		fedex = new FirstClass();	
//		
//		System.out.println(fedex.getName() + " " + fedex.calculateRate(distance, weight));
		
		List <DeliveryDriver> deliveryOptions = new ArrayList <DeliveryDriver>();
		deliveryOptions.add(new FexEd());
		deliveryOptions.add(new FirstClass());
		deliveryOptions.add(new SecondClass());
		deliveryOptions.add(new ThirdClass());
		deliveryOptions.add(new SPU2DayDelivery());
		deliveryOptions.add(new SPU4DayDelivery());
		deliveryOptions.add(new SPUNextDayDelivery());
		for (DeliveryDriver carrier : deliveryOptions) {
		System.out.println(carrier.getName() + " " + carrier.calculateRate(distance, weight));
		}
		
	
	
	} 

}
