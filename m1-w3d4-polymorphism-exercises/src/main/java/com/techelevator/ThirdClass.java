package com.techelevator;

public class ThirdClass implements DeliveryDriver {
	
	private double rate;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Postal Service Third Class";
	}

	@Override
	public double getDistance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double calculateRate(double distance, double weight) {
		
		if (weight <= 2){
			rate = distance * 0.0020;
		}
		if (weight > 2 && weight < 9) {
			rate = distance * 0.0022;
		}
		if (weight >= 9 && weight < 16) {
			rate = distance * 0.0024;
		}
		if (weight >= 16 && weight < 49) {
			rate = distance * 0.0150;
		}
		if (weight >= 49 && weight < 129) {
			rate = distance * 0.0160;
		}
		if (weight >= 129) {
			rate = distance * 0.0170;
		}
		
			return rate;
	
	}
	
	// |--------|-----------|-----------|-----------|
		// | | 1st Class | 2nd Class | 3rd Class |
		// | Ounces | Per Mile | Per Mile | Per Mile |
		// |--------|-----------|-----------|-----------|
		// | 0 - 2 | 0.035 | 0.0035 | 0.0020 |
		// | 3 - 8 | 0.040 | 0.0040 | 0.0022 |
		// | 9 - 15 | 0.047 | 0.0047 | 0.0024 |
		// | Pounds | | | |
		// | 1 - 3 | 0.195 | 0.0195 | 0.0150 |
		// | 4 - 8 | 0.450 | 0.0450 | 0.0160 |
		// | 9+ | 0.500 | 0.0500 | 0.0170 |
		// |--------|-----------|-----------|-----------|
		//
		//
		// rate = per mile rate * distance

	
	}


