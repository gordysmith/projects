package com.techelevator.vehicle;

public class Car implements Vehicle {
	
	//member variables
	private Boolean hasTrailer;
	
	
	//constructor
	public Car( boolean hasTrailer) {
		this.hasTrailer = hasTrailer;
		
	}
	//methods


	@Override
	public String getName() {
		String name = "Car";
		return name;
	}

	@Override
	public double calculateToll(int distance) {
		if (!hasTrailer) {
			return distance * 0.020;
		}
			if(hasTrailer) {
			return distance * 0.020 + 1.00;
		}
			return 0.00;
		
		
	}

}
