package com.techelevator.vehicle;

public interface Vehicle {
	
	/***
	 * calculates a Toll based on miles driven.
	 * 
	 * @param distance - distance in miles
	 * @return 
	 */
	double calculateToll(int distance); 
	
	String getName();

}
