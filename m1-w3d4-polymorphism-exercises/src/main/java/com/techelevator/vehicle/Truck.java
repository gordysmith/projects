package com.techelevator.vehicle;

import java.math.BigDecimal;

public class Truck implements Vehicle {

	//member variables
 
	
	private int axels;
	
	
	//Constructor
	public Truck(int axels) {
		this.axels = axels;
	}
	
	//methods
	

	@Override
	public String getName() {
		String name = "Truck";
		return name;
	}

	@Override
	public double calculateToll(int distance) {
		double calculatedToll = 0;
		
		if (axels == 4) {
			calculatedToll =distance * 0.040;
		}
		if (axels == 6) {
			calculatedToll = distance * 0.045;
		
		}
		if (axels >= 8) {
			calculatedToll = distance * 0.048;
			
		}
		return BigDecimal.valueOf(calculatedToll).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();

	}

}
