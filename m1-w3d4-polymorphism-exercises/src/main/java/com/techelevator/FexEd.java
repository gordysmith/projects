package com.techelevator;

public class FexEd implements DeliveryDriver {
	
	
	private double rate;
	private double over500Miles = 5.00;
	private double over48Ounces = 3.00; 
	
	
	
	@Override
	public String getName() {
		return "FedEx";
	}

	@Override
	public double calculateRate(double distance, double weight) {
		if (distance <= 500 && weight <=48) {
			rate = 20.00;
		}
		if (distance > 500 && weight <=48) {
			rate = 20.00 + over500Miles;
		}
		if (distance <= 500 && weight > 48) {
			rate = 20.00 + over48Ounces;
		}
		if (distance > 500 && weight > 48) {
			rate = 20.00 + over48Ounces + over500Miles;
		}
		return rate;
		
	
	}

	@Override
	public double getDistance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return 0;
	}
}
