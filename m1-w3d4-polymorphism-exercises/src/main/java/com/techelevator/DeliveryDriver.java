package com.techelevator;

public interface DeliveryDriver {
	
	String getName(); 
	double getDistance();
	double getWeight();
	
	double calculateRate(double distance, double weight);
	
	
}
