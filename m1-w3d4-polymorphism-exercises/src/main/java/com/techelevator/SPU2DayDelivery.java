package com.techelevator;

public class SPU2DayDelivery implements DeliveryDriver {
	
	private double rate;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "UPS 2-Day";
	}

	@Override
	public double getDistance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double calculateRate(double distance, double weight) {
		rate = (((weight/16)  * 0.050) * distance);
		return rate;
	}

}
