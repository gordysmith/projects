package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

import com.techelevator.vehicle.Tank;

public class TankTest {
	
	@Test
	public void checking_tank_toll() {
		Tank tank = new Tank();
		Assert.assertEquals(0, tank.calculateToll(50), 0);
	}

}
