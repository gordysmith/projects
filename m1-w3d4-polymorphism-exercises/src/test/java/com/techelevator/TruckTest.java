package com.techelevator;

import org.junit.Test;

import com.techelevator.vehicle.Truck;

import org.junit.Assert;

public class TruckTest {
	
	@Test
	public void checking_4_axels() {
		Truck truck = new Truck (4);
		Assert.assertEquals(0.40, truck.calculateToll(10), 0);
		
	}
	
	@Test
	public void checking_6_axels() {
		Truck truck = new Truck(6);
		Assert.assertEquals(0.45, truck.calculateToll(10), 0);
	}
	
	@Test
	public void checking_for_8_or_more_axels() {
		Truck truck = new Truck(8);
		Assert.assertEquals(0.48, truck.calculateToll(10), 0);
	}
	
	

}
