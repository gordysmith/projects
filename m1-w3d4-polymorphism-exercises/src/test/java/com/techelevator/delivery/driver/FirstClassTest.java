package com.techelevator.delivery.driver;

import org.junit.Test;

import com.techelevator.DeliveryDriver;
import com.techelevator.FirstClass;

import org.junit.Assert;

public class FirstClassTest {
	
	@Test
	public void check_first_class_rates_under_2_ounces() {
		DeliveryDriver firstClass = new FirstClass();
		Assert.assertEquals(0.35, firstClass.calculateRate(10, 1), 0.001);	
	}
	
	@Test
	public void check_first_class_rates_over_2_ounces_and_under_less_than_9_ounces() {
		DeliveryDriver firstClass = new FirstClass();
		Assert.assertEquals(0.40, firstClass.calculateRate(10, 5), 0.001);	
	}
	
	@Test
	public void check_first_class_rates_over_9_ounces_and_under_less_than_16_ounces() {
		DeliveryDriver firstClass = new FirstClass();
		Assert.assertEquals(0.47, firstClass.calculateRate(10, 12), 0.001);	
	}
	
	@Test
	public void check_first_class_rates_over_16_ounces_and_under_less_than_49_ounces() {
		DeliveryDriver firstClass = new FirstClass();
		Assert.assertEquals(1.95, firstClass.calculateRate(10, 25), 0.001);	
	}
	
	@Test
	public void check_first_class_rates_between_62_and_128_ounces() {
		DeliveryDriver firstClass = new FirstClass();
		Assert.assertEquals(4.50, firstClass.calculateRate(10, 72), 0.001);
	}
	
	@Test
	public void check_first_class_rates_over_128_ounces() {
		DeliveryDriver firstClass = new FirstClass();
		Assert.assertEquals(5.00, firstClass.calculateRate(10, 202), 0.001);
	}
	
	
	

}
