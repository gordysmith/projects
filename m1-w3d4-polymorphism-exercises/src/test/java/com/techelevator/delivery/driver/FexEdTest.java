package com.techelevator.delivery.driver;

import org.junit.Test;

import com.techelevator.DeliveryDriver;
import com.techelevator.FexEd;

import org.junit.Assert;

public class FexEdTest {
	
	@Test
	public void checking_rate_under_500_miles_and_under_48_ounces() {
		DeliveryDriver fexed = new FexEd();
		Assert.assertEquals(20.00, fexed.calculateRate(455, 36), 0);
	}
	
	@Test 
	public void checking_rate_over_500_miles_and_under_48_ounces() {
		DeliveryDriver fexed = new FexEd();
		Assert.assertEquals(25.00, fexed.calculateRate(555, 36), 0);
	}
	
	@Test
	public void checking_rate_under_500_miles_and_over_48_ounces() {
		DeliveryDriver fexed = new FexEd();
		Assert.assertEquals(23.00, fexed.calculateRate(325, 68), 0);
	}
	
	@Test
	public void checking_rate_over_500_miles_and_over_48_ounces() {
		DeliveryDriver fexed = new FexEd();
		Assert.assertEquals(28.00, fexed.calculateRate(750, 75), 0);
	}

}
