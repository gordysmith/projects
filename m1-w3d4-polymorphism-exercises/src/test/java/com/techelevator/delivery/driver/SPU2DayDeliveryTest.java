package com.techelevator.delivery.driver;

import org.junit.Assert;
import org.junit.Test;

import com.techelevator.DeliveryDriver;
import com.techelevator.SPU2DayDelivery;


public class SPU2DayDeliveryTest {

	
	@Test
	public void check_2_day_delivery_rate() {
		DeliveryDriver spu = new SPU2DayDelivery();
		Assert.assertEquals(5.00, spu.calculateRate(10, 10), 0);
	}
}
