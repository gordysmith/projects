package com.techelevator.delivery.driver;

import org.junit.Assert;
import org.junit.Test;

import com.techelevator.DeliveryDriver;
import com.techelevator.SPUNextDayDelivery;

public class SPUNextDayDeliveryTest {
	
	@Test
	public void check_next_day_delivery_rate() {
		DeliveryDriver spu = new SPUNextDayDelivery();
		Assert.assertEquals(7.50, spu.calculateRate(10, 10), 0);
	}

}
