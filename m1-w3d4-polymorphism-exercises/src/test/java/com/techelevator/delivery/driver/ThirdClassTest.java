package com.techelevator.delivery.driver;

import org.junit.Assert;
import org.junit.Test;

import com.techelevator.DeliveryDriver;
import com.techelevator.ThirdClass;


public class ThirdClassTest {
	
	@Test
	public void check_third_class_rates_under_2_ounces() {
		DeliveryDriver thirdClass = new ThirdClass();
		Assert.assertEquals(0.20, thirdClass.calculateRate(100, 1), 0.001);	
	}
	
	@Test
	public void check_third_class_rates_over_2_ounces_and_under_less_than_9_ounces() {
		DeliveryDriver thirdClass = new ThirdClass();
		Assert.assertEquals(0.22, thirdClass.calculateRate(100, 5), 0.001);	
	}
	
	@Test
	public void check_third_class_rates_over_9_ounces_and_under_less_than_16_ounces() {
		DeliveryDriver thirdClass = new ThirdClass();
		Assert.assertEquals(0.24, thirdClass.calculateRate(100, 12), 0.001);	
	}
	
	@Test
	public void check_third_class_rates_over_16_ounces_and_under_less_than_49_ounces() {
		DeliveryDriver thirdClass = new ThirdClass();
		Assert.assertEquals(1.50, thirdClass.calculateRate(100, 25), 0.001);	
	}
	
	@Test
	public void check_third_class_rates_between_62_and_128_ounces() {
		DeliveryDriver thirdClass = new ThirdClass();
		Assert.assertEquals(1.60, thirdClass.calculateRate(100, 72), 0.001);
	}
	
	@Test
	public void check_third_class_rates_over_128_ounces() {
		DeliveryDriver thirdClass = new ThirdClass();
		Assert.assertEquals(1.70, thirdClass.calculateRate(100, 202), 0.001);
	}


}
