package com.techelevator.delivery.driver;

import org.junit.Assert;
import org.junit.Test;

import com.techelevator.DeliveryDriver;

import com.techelevator.SecondClass;

public class SecondClassTest {
	
	@Test
	public void check_second_class_rates_under_2_ounces() {
		DeliveryDriver secondClass = new SecondClass();
		Assert.assertEquals(0.35, secondClass.calculateRate(100, 1), 0.001);	
	}
	
	@Test
	public void check_second_class_rates_over_2_ounces_and_under_less_than_9_ounces() {
		DeliveryDriver secondClass = new SecondClass();
		Assert.assertEquals(0.40, secondClass.calculateRate(100, 5), 0.001);	
	}
	
	@Test
	public void check_seond_class_rates_over_9_ounces_and_under_less_than_16_ounces() {
		DeliveryDriver secondClass = new SecondClass();
		Assert.assertEquals(0.47, secondClass.calculateRate(100, 12), 0.001);	
	}
	
	@Test
	public void check_second_class_rates_over_16_ounces_and_under_less_than_49_ounces() {
		DeliveryDriver secondClass = new SecondClass();
		Assert.assertEquals(1.95, secondClass.calculateRate(100, 25), 0.001);	
	}
	
	@Test
	public void check_second_class_rates_between_62_and_128_ounces() {
		DeliveryDriver secondClass = new SecondClass();
		Assert.assertEquals(4.50, secondClass.calculateRate(100, 72), 0.001);
	}
	
	@Test
	public void check_second_class_rates_over_128_ounces() {
		DeliveryDriver secondClass = new SecondClass();
		Assert.assertEquals(5.00, secondClass.calculateRate(100, 202), 0.001);
	}

}
