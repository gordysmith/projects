package com.techelevator.delivery.driver;

import org.junit.Test;

import com.techelevator.DeliveryDriver;
import com.techelevator.SPU4DayDelivery;

import org.junit.Assert;

public class SPU4DayDeliveryTest {
	
	@Test
	public void checking_4_day_ground_rate() {
		DeliveryDriver spu = new SPU4DayDelivery();
		Assert.assertEquals(.50, spu.calculateRate(10, 10), 0);
	}
	

}
