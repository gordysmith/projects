package com.techelevator;

import org.junit.Test;

import com.techelevator.vehicle.Car;

import org.junit.Assert;

public class CarTest {
	
	@Test
	public void checking_car_without_trailer_toll() {
		Car car = new Car(false);
		Assert.assertEquals(0.20, car.calculateToll(10), 0);
	}
	
	@Test
	public void checking_car_with_trailer_toll() {
		Car car = new Car(true);
		Assert.assertEquals(1.20, car.calculateToll(10), 0);
	}

}
