package com.techelevator;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class Example {

	public static void main(String[] args) {
		
		List<Double> numbers = new ArrayList<Double>();
		
		numbers.add(2.4d);
		
		numbers.add(new Double(1.5));
		
		double sum = 0;
		for (int i = 0; i <numbers.size(); i++) {
			sum += numbers.get(i);
		}
		
		System.out.println("Sum of Double List: " + sum);

	
	List<String> names = new ArrayList<String>();
	//names.add("Kirk");
	
	
	int [] ints = new int [] { 1, 2, 3, 4 } ;
	
	//int sumOfEvens = 0
	
	//for (int number : ints) {
		
		//if (number % 2 == 0) {
		//	sumeOfEvens +=number;
		//}
		//System.out.println("Sum of Evens: " + sumOfEvens);
	
	//}
	
	Queue<String> queue = new LinkedList<String>();
	queue.offer("John");
	queue.offer("Jane");
	queue.offer("Sue");
	
	for (String name : queue) {
		System.out.println(queue.size() + " : " + name);
	}
	
	while(!queue.isEmpty()) {
		System.out.println(queue.size() + " : " + queue.poll());
	}
		
	
	Stack<Integer> nums = new Stack<Integer>();
	nums.push(10);
	nums.push(20);
	nums.push(30);
	
	while (!nums.isEmpty()) {
		System.out.println(nums.size() + " : " + nums.pop());
	}

}
}
