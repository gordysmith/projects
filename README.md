# Gordy Smith's Projects

While attending Tech Elevator in the Summer of 2018 these are some of the projects I worked on

## MVC Java Capstone (m3-java-capstone)

This was a pair project in which my partner and I built a full-stack National Parks Weather Service application that utilized a local database. I worked mainly on the back-end while my partner built the JSP's and CSS on the front-end. 

## Controller Exercises (m3-w8d2-controllers_part1_exercises)

This was an individual exercise where I created a controller to handle requests to search an actor table, customer table, and films table on a local database. I built the JSP's as well to display the various search forms.

## Sessions Exercise (m3-w8d4-sessions_exercises)

This was an individual exercise where I built a web application for users to take a survey about their favorite things across 4 separate pages. I learned how to store their "favorites" in SessionAttributes so that their choices would be saved in Session until they had completed the survey and had seen their results. 




