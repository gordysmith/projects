package com.techelevator;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.techelevator.dao.ActorDao;
import com.techelevator.dao.model.Actor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller 
public class ActorSearchController {

	@Autowired
	private ActorDao actorDao;

	@RequestMapping("/actorSearchForm")
	public String showSearchActorForm() {
		return "actorSearchForm";
	}

	@RequestMapping("/actorSearch")
	public String searchActors(HttpServletRequest request) {
		String name = request.getParameter("lastName");             //gets parameter from request from url
		List <Actor> actors = actorDao.getMatchingActors(name);     //calls method from dao and searchs name from request and puts it in List
		request.setAttribute("actors", actors);                     //sets the results in list to a variable to be used in the view (actorList.jsp)
		return "actorList";                                         //returns the name of jsp to display
	}
}
