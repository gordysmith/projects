package com.techelevator.dog;

public class Dog {
	
	//member variables
	private boolean isSleeping;

	//getter
	public boolean isSleeping() {
		return isSleeping;
	}
	
	//methods
	public String makeSound() {
		if (this.isSleeping()) {
			return "Zzzzz...";
		} else {
			return "Woof!";
		}
	}
	public void sleep() {
		this.isSleeping = true;
		
	}
	public void wakeUp() {
		this.isSleeping = false;
		
	}
	

	//
	// Write code here
	//
	
}
