package com.techelevator.calculator;

public class Calculator {
	
	
	private int currentValue;

	//getter
	public int getCurrentValue() {
		return currentValue;
	}
	
	//methods
	public int add(int addend) {
		currentValue = this.currentValue + addend;
		return currentValue;
		
		
	}
	public int multiply(int multiplier) {
		currentValue = this.currentValue * multiplier;
		return currentValue;
		
	}
	public int subtract(int subtrahend) {
		currentValue = this.currentValue - subtrahend;
		return currentValue;
	}
	
	public int power(int exponent) {
		currentValue = (int) Math.pow(currentValue, exponent);
		return currentValue;
		
	}
	public void reset() {
		currentValue = 0;
		
		
	}
	
	

	//
	// Write code here
	//
	
}
