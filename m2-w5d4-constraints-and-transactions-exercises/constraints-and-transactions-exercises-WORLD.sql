-- Write queries to return the following:
-- Make the following changes in the "world" database.

-- 1. Add Superman's hometown, Smallville, Kansas to the city table. The 
-- countrycode is 'USA', and population of 45001. (Yes, I looked it up on 
-- Wikipedia.)
select count(name) from city 
insert into city (id, name, countrycode, district, population)
values ('4080','Smallville', 'USA', 'Kansas', '45001')
select * from city
where id = 4080

-- 2. Add Kryptonese to the countrylanguage table. Kryptonese is spoken by 0.0001
-- percentage of the 'USA' population.
select * from countrylanguage
insert into countrylanguage (countrycode, language, isofficial, percentage)
values ('USA', 'Kryptonese', false, '0.0001')
select * from countrylanguage
where countrycode = 'USA'

-- 3. After heated debate, "Kryptonese" was renamed to "Krypto-babble", change 
-- the appropriate record accordingly.

update countrylanguage
set language = 'Kryptobabble'
where language = 'Krypto-nese'

select * from countrylanguage
where countrycode = 'USA'





-- 4. Set the US captial to Smallville, Kansas in the country table.
select capital from country
where code = 'USA'
start transaction
commit
rollback
update country
set capital = '4080'
where capital = '3813'


-- 5. Delete Smallville, Kansas from the city table. (Did it succeed? Why?)
select name from city
where countrycode = 'USA'
start transaction
commit 
rollback
delete from city
where name = 'Smallville'
--Won't let me do it because Smallville is a foreign key on the country table.

-- 6. Return the US captial to Washington.
select capital from country
where code = 'USA'
start transaction
commit
rollback
update country
set capital = '3813'
where capital = '4080'


-- 7. Delete Smallville, Kansas from the city table. (Did it succeed? Why?)
select name from city
where countrycode = 'USA'
start transaction
commit 
rollback
delete from city
where name = 'Smallville'
--It worked because Smallville is no longer a foreign key on the country table.


-- 8. Reverse the "is the official language" setting for all languages where the
-- country's year of independence is within the range of 1800 and 1972 
-- (exclusive). 
-- (590 rows affected)
select * from country
where indepyear > 1800 and indepyear < 1972
start transaction
commit 
rollback
update countrylanguage
set isofficial = not isofficial
where countrycode in (select code from country where indepyear > 1800 and indepyear < 1972)

-- 9. Convert population so it is expressed in 1,000s for all cities. (Round to
-- the nearest integer value greater than 0.)
-- (4079 rows affected)
select * from city
start transaction
commit
rollback
update city.population 
set city.population = city.population / 1000
where city.population = city.population

-- 10. Assuming a country's surfacearea is expressed in miles, convert it to 
-- meters for all countries where French is spoken by more than 20% of the 
-- population.
-- (7 rows affected)
