<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="pageTitle" value="Home Page"/>
<%@include file="common/header.jspf" %>

	<h2>New Registration Form</h2>
	
	
	 
		<c:url var = "registration" value = "/registration" />
		<form:form action = "${registration}" method = "POST" modelAttribute = "registration">
		
			<div>
				<label for = "firstName">First Name</label>
				<form:input path = "firstName" />
				<form:errors path = "firstName" />
			</div>	
			<br/>
			<br/>
			
			<div>
				<label for = "lastName">Last Name</label>
				<form:input path = "lastName" />
				<form:errors path = "lastName" />
			</div>
			<br/>
			<br/>
			
			<div>
				<label for = "email">Email</label>
				<form:input path = "email" />
				<form:errors path = "email" />
			</div>
			<br/>
			<br/>
			
			<div>
				<label for = "verifyEmail">Confirm Email</label>
				<form:input path = "verifyEmail" />
				<form:errors path = "verifyEmail" />
			</div>
			<br/>
			<br/>
			
			<div>
				<label for = "password">Password</label>
				<form:input path = "password" />
				<form:errors path = "password" />
			</div>
			<br/>
			<br/>
			
			<div>
				<label for = "verifyPassword">Confirm Password</label>
				<form:input path = "verifyPassword" />
				<form:errors path = "verifyPassword" />
			</div>
			<br/>
			<br/>
			
			<div>
				<label for = "birthday">Birthday</label>
				<form:input path = "birthday" />
				<form:errors path = "birthday" />
			</div>
			<br/>
			<br/>
			
			<div>
				<label for = "ticketsDesired">Number of Tickets</label>
				<form:input path = "ticketsDesired" />
				<form:errors path = "ticketsDesired" />
			</div>
			<br/>
			<br/>
			<input type = "submit" value = "SUBMIT"/>
			
		</form:form >