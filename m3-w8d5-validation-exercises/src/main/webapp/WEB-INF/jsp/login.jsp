<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="pageTitle" value="Login Page"/>
<%@include file="common/header.jspf" %>

	<h2>Login Page</h2>
	
		<c:url var = "login" value = "/login" />
		<form:form action = "${login}" method = "POST" modelAttribute = "login" >
		
			<div>
				<label for = "loginEmail">Email</label>
				<form:input path = "loginEmail" />
				<form:errors path = "loginEmail" />
			</div>
			
			<br/>
			<br/>
			
			<div>
				<label for = "loginPassword">Password</label>
				<form:input path="loginPassword"/>
				<form:errors path="loginPassword"/>
			</div>
			
			<br/>
			<br/>
			
			<input type = "submit" value = "SUBMIT"/>
		
		</form:form>