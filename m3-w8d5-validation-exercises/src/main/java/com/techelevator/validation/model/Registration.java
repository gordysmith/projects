package com.techelevator.validation.model;

import java.time.LocalDate;
import java.util.Date;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

public class Registration {
	
	@NotBlank(message="First name is required")
	@Size(min=1, max=20, message="First name must not exceed 20 characters")
	private String firstName;
	
	@NotBlank(message="Last name is required")
	@Size(min=1, max=20, message="Last name must not exceed 20 characters")
	private String lastName;
	
	@NotBlank(message="Email address is required")
	@Email(message="Valid email address is required")
	private String email;
	
	@NotBlank(message="Verify email address is required")
	private String verifyEmail;
	
	@NotBlank(message="Password is required")
	@Size(min=8, message="Password must be at least 8 characters")
	private String password;
	
	@NotBlank(message="Verify password is required")
	private String verifyPassword;
	
	@Past(message="Please enter a valid birth date")
	Date birthday;
	
	@NotBlank(message="Please enter how many tickets you would like")
	@Size(min=1, max=10, message="Range available for purchase is between 1 - 10 tickets")
	private String ticketsDesired;
	
	private boolean emailMatching;
	@AssertTrue(message="Emails must match")
	public boolean isEmailMatching() {
		if(email != null) {
			return email.equals(verifyEmail);
		} else {
			return false;
		}
	}
	
	private boolean passwordMatching;
	@AssertTrue(message="Passwords must match")
	public boolean isPasswordMatching() {
		if(password != null) {
			return password.equals(verifyPassword);
		} else {
			return false;
		}
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVerifyEmail() {
		return verifyEmail;
	}
	public void setVerifyEmail(String verifyEmail) {
		this.verifyEmail = verifyEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVerifyPassword() {
		return verifyPassword;
	}
	public void setVerifyPassword(String verifyPassword) {
		this.verifyPassword = verifyPassword;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getTicketsDesired() {
		return ticketsDesired;
	}
	public void setTicketsDesired(String ticketsDesired) {
		this.ticketsDesired = ticketsDesired;
	}
	public void setEmailMatching(boolean emailMatching) {
		this.emailMatching = emailMatching;
	}
	public void setPasswordMatching(boolean passwordMatching) {
		this.passwordMatching = passwordMatching;
	}
	
	
	

}
