package com.techelevator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FizzWriter {
	public static void main(String[] args) throws IOException, InterruptedException {
	
		
		File FizzBuzz = new File("FizzBuzz.txt");
		
		if(!FizzBuzz.exists()) {
			FizzBuzz.createNewFile();
			
		
		} 
			

		
		
		try (PrintWriter writer = new PrintWriter(FizzBuzz)){
			
			
			for (int i = 0; i <= 300; i++) {
			
				if( i % 3 == 0 && i % 5 == 0) {
					writer.println("FizzBuzz");
				}
				else if ( i % 3 == 0  || containsDigit(3, i)) {
					writer.println("Fizz");
				}
				else if( i % 5 == 0 || containsDigit(5, i)) {
					writer.println("Buzz");
				}
				
				else writer.println(i);
			}
			
			
			
	

		}
	}
	
	private static boolean containsDigit(int digit, int number) {
		return String.valueOf(number).contains(String.valueOf(digit));
	}
}