package com.techelevator;

import java.util.Scanner;

/*
 The foot to meter conversion formula is:
 	m = f * 0.3048
 	
 The meter to foot conversion formula is:
 	f = m * 3.2808399
 	
 Write a command line program which prompts a user to enter a length, and whether the measurement is in (m)eters or (f)eet.
 Convert the length to the opposite measurement, and display the old and new measurements to the console.
  
 $ java LinearConvert
 Please enter the length: 58
 Is the measurement in (m)eter, or (f)eet? f
 58f is 17m.
 */

public class LinearConvert {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		String [] numChoice = new String [] {"m", "f"};
		
		System.out.println("Please enter a length:");
		String input = in.nextLine();
		double length = Double.parseDouble(input);
		double mConvertedToF = (length * 3.2808399);
		double fConvertedToM = (length * 0.3048);
		
		System.out.println("Please choose (m)eters or (f)eet:");
		String choice = in.nextLine();
		
		System.out.println("You entered: " + input + choice + "!");
		
		switch(choice) {
			case "m":
			System.out.println("Your length in feet is " + mConvertedToF + " feet!");
			break;
			
			case "f":
			System.out.println("Your length in meters is " +fConvertedToM + " meters!");
			
			default:
				break;
		}
		
		
		
		}

	}


