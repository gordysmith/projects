package com.techelevator;

import java.util.Scanner;

/*
Write a command line program which prompts the user for a series of decimal integer values  
and displays each decimal value as itself and its binary equivalent

$ DecimalToBinary 

Please enter in a series of decimal values (separated by spaces): 460 8218 1 31313 987654321

460 in binary is 111001100
8218 in binary is 10000000011010
1 in binary is 1
31313 in binary is 111101001010001
987654321 in binary is 111010110111100110100010110001
*/
public class DecimalToBinary {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("How many decimal values do you wish to enter?:");
		String numOfValues = in.nextLine();
		int totalValues = Integer.parseInt(numOfValues);
		
		String arrayOfValues [] = new String [totalValues];
		
		for (int i = 0; i <= arrayOfValues.length; i++) {
			System.out.println("Please enter a value:");
			arrayOfValues[i] = in.nextLine();
			System.out.println(arrayOfValues);
			
		}
		
		
		

	}

}
