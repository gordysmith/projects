package com.techelevator;

public class Airplane {

	private String planeNumber;
	private int bookedFirstClassSeats;
	private int availableFirstClassSeats;
	private int totalFirstClassSeats;
	private int bookedCoachSeats;
	private int availableCoachSeats;
	private int totalCoachSeats;

	public Airplane(String planeNumber, int totalFirstClassSeats, int totalCoachSeats){
		this.planeNumber = planeNumber;
		this.totalFirstClassSeats = totalFirstClassSeats;
		this.availableFirstClassSeats = totalFirstClassSeats;
		this.totalCoachSeats = totalCoachSeats;
		this.availableCoachSeats = totalCoachSeats;
	}

	public String getPlaneNumber() {
		return planeNumber;
	}
	public int getBookedFirstClassSeats() {
		return bookedFirstClassSeats;
	}
	public int getTotalFirstClassSeats() {
		return totalFirstClassSeats;
	}
	public int getBookedCoachSeats() {
		return bookedCoachSeats;
	}
	public int getTotalCoachSeats() {
		return totalCoachSeats;
	}

	public int getAvailableFirstClassSeats() {
		this.availableFirstClassSeats = this.totalFirstClassSeats - this.bookedFirstClassSeats;
		return this.availableFirstClassSeats;
	}

	public int getAvailableCoachSeats() {
		this.availableCoachSeats = this.totalCoachSeats - this.bookedCoachSeats;
		return this.availableCoachSeats;
	}

	public boolean reserveSeats(boolean forFirstClass, int totalNumberOfSeats) {


		if(forFirstClass) {
			if(this.availableFirstClassSeats >= totalNumberOfSeats) {
				this.bookedFirstClassSeats += totalNumberOfSeats;
				this.availableFirstClassSeats -= totalNumberOfSeats;
				return true;
			} else {
				return false;
			}
		}
			else {
				if(this.availableCoachSeats >= totalNumberOfSeats) {
					this.bookedCoachSeats += totalNumberOfSeats;
					this.availableCoachSeats -= totalNumberOfSeats;
					return true;
				} else {
					return false;
				}
			}

			//			if (getAvailableFirstClassSeats() >= totalNumberOfSeats && forFirstClass == true) {
			//				this.bookedFirstClassSeats += totalNumberOfSeats;
			//				return true;
			//			} else if (getAvailableCoachSeats() >= totalNumberOfSeats && forFirstClass == false) {
			//				this.bookedCoachSeats += totalNumberOfSeats;
			//				return true;
			//			} 
			//				
			//			return false;

		}



	}
