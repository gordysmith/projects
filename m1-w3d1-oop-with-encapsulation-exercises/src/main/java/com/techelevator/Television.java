package com.techelevator;

	

public class Television {
	
	private boolean isOn = false;
	private int currentChannel = 3;
	private int currentVolume = 2;
	
	
	public boolean isOn() {
		return isOn;
	}
	public int getCurrentChannel() {
		return currentChannel;
	}
	public int getCurrentVolume() {
		return currentVolume;
	}
	
	
//	//methods
	public void turnOff() {
		if (isOn)  {
		isOn = !isOn;
		} 
		
	}
	public void turnOn() {
		if (!isOn) {
		isOn = true;
		}
	}
	public void changeChannel(int newChannel) {
		if (isOn && (newChannel >=3 && newChannel <= 18)) {
			
				currentChannel = newChannel;	
		
		} 

		
	}
	public void channelUp() {
		if (isOn && currentChannel < 18) {
			currentChannel ++;
		} else {
			currentChannel = 3;
		}
	}
	
	public void channelDown() {
		if(isOn) {
			{
			if(currentChannel <= 18 && currentChannel > 3) {
			
			currentChannel --;
			} else {
				currentChannel = 18;
			}

			}
		}
	}
	public void raiseVolume() {
		if(isOn) {
			{ 
				if(currentVolume <= 9) {
					currentVolume ++;
				}
			}
			
		}
	}
	public void lowerVolume() {
		if(isOn) {
			{
				if (currentVolume > 0) {
					currentVolume --;
				}
			}
		
		
	}
	
	}

}
