package com.techelevator;

public enum TriangleClassification {
	EQUILATERAL, ISOCELES, SCALENE, INEQUALITY
}
