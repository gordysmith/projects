package com.techelevator;

import java.util.HashMap;
import java.util.Map;

public class KataPotter {
	
//	private String book1;
//	private String book2;
//	private String book3;
//	private String book4;
//	private String book5;
//	
//	
//	private double cost;
//	private double copies;
//	
	
	public double price = 8.00;
	int numOfCopies;
	double discount1 = 0.95;
	double discount2 = 0.90;
	double discount3 = 0.80;
	double discount4 = 0.75;
	
	
	
	public double getCost(int[] books) {
		Map<String, Integer> bookCollection = new HashMap<String, Integer>();
		Map<Integer, Double> discounts = new HashMap<Integer, Double>();
		
		
		if (books.length == 5 && books[0] == 1 && books[1] == 1 && books[2] == 1 && books[3] == 1 && books [4] == 1) {
			return 40.00 * discount4;
		}
		
		if (books.length == 5 && books[0] == 1 && books[1] == 1 && books[2] == 1 && books[3] == 1 && books [4] == 0) {
			return 32.00 * discount3;
		}
		
		if (books.length == 5 && books[0] == 1 && books[1] == 1 && books[2] == 1 && books[3] == 0 && books [4] == 0) {
			return 24.00 * discount2;
		}
		
		if (books.length == 5 && books[0] == 1 && books[1] == 1 && books[2] == 0 && books[3] == 0 && books [4] == 0) {
			return 16.00 * discount1;
		}
		
		if (books.length == 5 && books[0] == 1 && books[1] == 0 && books[2] == 0 && books[3] == 0 && books [4] == 0) {
			return 8.00;
		}
		
	
		
	
		return 0.00;
	}
	
	

}
