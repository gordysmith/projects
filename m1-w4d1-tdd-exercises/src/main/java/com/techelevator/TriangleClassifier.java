package com.techelevator;

public class TriangleClassifier {

	public TriangleClassification classify(int len1, int len2, int len3) {
		if ((len1 + len2 > len3) || (len1 + len3 > len2) || (len2 + len3 > len1)) {
			return TriangleClassification.INEQUALITY;
		}
		if ((len1 == len2 && len1 != len3) || (len1 == len3 && len1 != len2) || (len2 == len3 && len1 != len3)) {
			return TriangleClassification.ISOCELES;
		}
		if (len1 != len2 && len1 != len3 && len2 != len3) {
			return TriangleClassification.SCALENE;
		}
		
		return TriangleClassification.EQUILATERAL;
	}

}
