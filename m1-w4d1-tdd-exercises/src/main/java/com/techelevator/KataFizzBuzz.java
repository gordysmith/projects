package com.techelevator;

/***
 * 
 * @author brianlauvray
 *
 */
public class KataFizzBuzz {
	
	private final static int FIZZABLE = 3;
	private final static int BUZZABLE = 5;
	private final static int RANGE_FLOOR = 1;
	private final static int RANGE_CEILING = 100;
	private final static String FIZZ = "Fizz";
	private final static String BUZZ = "Buzz";
	
	public String fizzBuzz(int value) {
		
		String returnValue = "";
		
		if (value < RANGE_FLOOR || value > RANGE_CEILING) {
			return returnValue;
		}
		


		if (isDivisibleBy(FIZZABLE, value) && isDivisibleBy(BUZZABLE, value)) {
			return "FizzBuzz";
		}
		
		if (isDivisibleBy(FIZZABLE, value) && containsDigit(BUZZABLE, value)) {
			return "FizzBuzz";
		}
		
		if(isDivisibleBy(BUZZABLE, value) && containsDigit(FIZZABLE, value)) {
			return "FizzBuzz";
		}
		
		
		if (isDivisibleBy(FIZZABLE, value)  || (containsDigit(FIZZABLE, value))) {
			returnValue += FIZZ;
		}
		
		if (isDivisibleBy(BUZZABLE, value) || (containsDigit(BUZZABLE, value))) {
			returnValue += BUZZ;
		}
		
//		if (containsDigit(3, value)) {
//			return "Fizz";
//		}
//		
//		if (containsDigit(5, value)) {
//			return "Buzz";
//		}
		
	
		
	
//		if (returnValue.length() < 1) {
//			return String.valueOf(value);
//		} else {
//			return returnValue;
//		}
		
		return returnValue.length() < 1 ? String.valueOf(value) : returnValue;
		//return String.valueOf(value);
	}


	
	/***
	 * 
	 * @param value
	 * @return
	 */

	

	private boolean isDivisibleBy(int multiple, int number) {
		return number % multiple == 0;
	}
	
	private boolean containsDigit(int digit, int number) {
		return String.valueOf(number).contains(String.valueOf(digit));
	}

}
