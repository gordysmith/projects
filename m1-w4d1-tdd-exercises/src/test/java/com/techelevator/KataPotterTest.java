package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataPotterTest {
	
	KataPotter potter;
	
	@Before
	public void setup() {
		potter = new KataPotter();
	}
	
	@Test
	public void do_all_five_books_get_right_discount () {
		Assert.assertEquals(30.00, potter.getCost(new int[] {1, 1, 1, 1, 1}), 0.001);
	}
	
	@Test
	public void do_four_books_get_right_discount() {
		Assert.assertEquals(25.60, potter.getCost(new int [] {1, 1, 1, 1, 0}), 0.001);
	}
	
	@Test 
	public void do_three_books_get_right_discount() {
		Assert.assertEquals(21.60, potter.getCost(new int [] {1, 1, 1, 0, 0}), 0.001);
	}
	
	@Test
	public void do_two_originals_equal_right_price() {
		Assert.assertEquals(15.20, potter.getCost(new int[] {1, 1, 0, 0, 0}), 0.001);
	}

	@Test
	public void does_first_copy_equal_8_dollars(){
		Assert.assertEquals(8.00, potter.getCost(new int[] {1, 0, 0, 0, 0 }), 0.001);
	}
	

}

