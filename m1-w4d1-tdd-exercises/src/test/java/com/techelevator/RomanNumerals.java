package com.techelevator;

public class RomanNumerals {

	public String [] romanNumbers = {"I", "II", "III", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"};
	public int [] arabicNumbers = {1, 2, 3, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000};
	public String results = "";

	public String arabicToRoman(int arabic) {
		if (arabic <= 0) {
			return "invalid number";
		}
		for (int i = arabicNumbers.length -1; i >= 0; i--) {
			while(arabic >= arabicNumbers[i]) {
				results += romanNumbers[i];
				arabic -= arabicNumbers[i];

			}
		}
		return results;
	}


	public  void main (String[] args) {

		System.out.print(arabicToRoman(55));
	}

	public static String oneToI(int arabic) {
		return "I";
	}

	public static String twoToII(int arabic) {
		return "II";
	}

	public static String threeToIII(int arabic) {
		return "III";
	}


	public static String fourToIV(int arabic) {
		return "IV";
	}

	public static String fiveToV(int arabic) {
		return "V";
	}

	public static String nineToIX(int arabic) {
		return "IX";
	}

	public static String tenToX(int arabic) {
		return "X";
	}

	public static String fortyToXL(int arabic) {
		return "XL";
	}

	public static String fiftyToL(int arabic) {
		return "L";
	}

	public static String nintyToXC(int arabic) {
		return "XC";
	}

	public static String oneHundredToC(int arabic) {
		return "C";
	}

	public static String fiveHundredToD(int arabic) {
		return "D";
	}

	public static String oneThousandToM(int arabic) {
		return "M";
	}







}

