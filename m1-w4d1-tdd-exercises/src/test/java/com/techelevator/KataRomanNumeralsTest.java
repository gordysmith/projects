package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataRomanNumeralsTest {
	
@Test
public void one_To_I() {
	Assert.assertEquals("1", "I", RomanNumerals.oneToI(1));
	
}

@Test
public void two_To_II() {
	Assert.assertEquals("2", "II", RomanNumerals.twoToII(2));
}

@Test
public void three_To_III() {
	Assert.assertEquals("3", "III", RomanNumerals.threeToIII(3));
}

@Test
public void four_To_IV() {
	Assert.assertEquals("4", "IV", RomanNumerals.fourToIV(4));
}

@Test
public void five_To_V() {
	Assert.assertEquals("5", "V", RomanNumerals.fiveToV(5));
}

@Test
public void nine_To_IX() {
	Assert.assertEquals("9", "IX", RomanNumerals.nineToIX(9));
}

@Test
public void ten_to_X() {
	Assert.assertEquals("10", "X", RomanNumerals.tenToX(10));
}

@Test
public void forty_to_XL() {
	Assert.assertEquals("40", "XL", RomanNumerals.fortyToXL(40));
	
}

@Test
public void fify_To_L() {
	Assert.assertEquals("50", "L", RomanNumerals.fiftyToL(50));
}

@Test
public void ninty_to_XC() {
	Assert.assertEquals("90", "XC", RomanNumerals.nintyToXC(90));
}

@Test
public void one_hundred_to_C() {
	Assert.assertEquals("100", "C", RomanNumerals.oneHundredToC(100));
}

@Test
public void five_hundred_to_D() {
	Assert.assertEquals("500", "D", RomanNumerals.fiveHundredToD(500));
}

@Test 
public void one_thousand_to_M() {
	Assert.assertEquals("1000", "M", RomanNumerals.oneThousandToM(1000));
}

@Test
public void convert_1_thur_3 () {
	
}

}
