package com.techelevator;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class TriangleClassifierTest {

	private TriangleClassifier triangleClassifier;
	
	@Before
	public void setup() {
		triangleClassifier = new TriangleClassifier();
	}
	
	@Test
	public void all_sides_must_be_positive () {
		Assert.fail("Need to implement validation");
	}
	
	@Test
	public void triangle_is_equilateral_if_all_sides_are_equal () {
		TriangleClassification result = triangleClassifier.classify(3, 3, 3);
		Assert.assertEquals(TriangleClassification.EQUILATERAL, result);
	}
	
	@Test
	public void triangle_is_isoceles_if_two_sides_are_equal() {
		TriangleClassification result = triangleClassifier.classify(1, 2, 2);
		Assert.assertEquals(TriangleClassification.ISOCELES, result);
		
		result = triangleClassifier.classify(2, 1, 2);
		Assert.assertEquals(TriangleClassification.ISOCELES, result);
		
		result = triangleClassifier.classify(2, 2, 1);
		Assert.assertEquals(TriangleClassification.ISOCELES, result);
	}
	
	@Test
	public void triangle_is_scalene_if_no_sides_are_equal () {
		TriangleClassification result = triangleClassifier.classify(1, 2, 3);
		Assert.assertEquals(TriangleClassification.SCALENE, result);
	}
	
	@Test
	public void triangle_does_not_violate_inequality () {
		TriangleClassification result = triangleClassifier.classify(4, 5, 8);
		Assert.assertEquals(TriangleClassification.INEQUALITY, result);
	}
	
}
